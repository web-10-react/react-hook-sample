import './App.css';
import ItemCard from './components/ItemCard';
import "bootstrap/dist/css/bootstrap.min.css"
import { AllUsers } from './pages/AllUsers';
import MyNavBar from './components/MyNarBar';
import { AllPhotos } from './pages/AllPhotos';
import { BrowserRouter as BigRouter, Route, Routes } from 'react-router-dom'; 
import HomePage from './components/HomePage';
import NewPhotos from './pages/NewPhotos';
import NotFoundPage from './pages/NotFoundPage';
function App() {

  
  return (
    <div  >
      <MyNavBar/> 
      <BigRouter>
        <Routes>
            <Route path='/' index element={<HomePage/>}/>
            <Route path='/users'  element={<AllUsers/>}/>
            <Route path='/photos' element={<NewPhotos/>}/>
            <Route path='*'  element={<NotFoundPage/>}/>
            
        </Routes>
      </BigRouter>
    </div>
  );
}

export default App;
